﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repeticion
{
    class Program
    {
        static void Main(string[] args)
        {
            int a;
            Console.WriteLine("Ingrese numero de ejercicio");
            a = Convert.ToInt32(Console.ReadLine());
            if (a == 1)
            {
                Proceso m = new Proceso();
                m.Ejercicio1();
            }
            if (a == 2)
            { //Ejercicio 2
                Proceso m = new Proceso();
                m.Ejercicio2();
            }

            if (a == 3)
            { //Ejercicio 3
                Proceso m = new Proceso();
                m.Ejercicio3();
            }
            if (a == 4)
            { //Ejercicio 4
                Proceso m = new Proceso();
                m.Ejercicio4();
            }
            if (a == 5)
            { //Ejercicio 5
                Proceso m = new Proceso();
                m.Ejercicio5();

            }
        }
    }
}
